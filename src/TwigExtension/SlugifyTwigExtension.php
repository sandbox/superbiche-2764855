<?php

namespace Drupal\twig_slugify\TwigExtension;

use Cocur\Slugify\Slugify;

/**
 * Class Slugify.
 *
 * @package Drupal\twig_slugify
 */
class SlugifyTwigExtension extends \Twig_Extension {

  
  /**
   * {@inheritdoc}
   */
  public function getName() {
    return 'twig_slugify.slugify';
  }

  public function getFilters() {
    return [
      'slugify' => new \Twig_SimpleFilter('slugify', [$this, 'slugify']),
    ];
  }

  public static function slugify($string) {
    $slugify = new Slugify();
    return $slugify->slugify($string);
  }
}
